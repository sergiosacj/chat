typedef struct chat_rooms {
  pthread_t thread_id;
  char members[100][100];
  int members_counter;
} ChatRoom;

#define MAX_LINE_SIZE 1024
#define STDIN 0
#define STOP_CONNECTION "/sair\n"
#define IP "0.0.0.0"
#define PORT 1200
#define ROOM_LIMIT 2
#define FULL_ROOM_WARNING "A sala está cheia, encerrando seu acesso.\n"
