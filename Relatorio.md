```
Disciplina: Fundamento de Redes de Computadores
Curso: Engenharia de Software (UnB)
professor: Fernando William Cruz
aluno: Sérgio de Almeida Cipriano Júnior (180037439)
```

# Introdução

O objetivo do projeto é criar um bate-papo virtual com múltiplas salas e clientes. A solução implementada gera o mesmo binário para o servidor e para o cliente. Esse executável recebe como parâmetro o modo de execução, podendo ser "client" ou "server", e baseado no modo abre um menu com algumas opções de ações.

O servidor pode criar salas, permitir o ingresso de clientes em salas existentes, permitir a saída de clientes de uma sala em que estava participando, auxiliar no diálogo entre os clientes de uma mesma sala e listar participantes de uma sala (essa funcionalidade não está totalmente funcional).

Os clientes podem criar um login/nickname, entrar numa sala baseado no endereço, sair de uma sala digitando o comando "/sair" e podem mandar e receber mensagens de outros participantes dentro de uma mesma sala.

# Como executar o projeto

Construir os binários:
```
$ make
```

Servidor:
```
$ make run-server
```

Cliente:
```
$ make run-client
```

# Metodologia utilizada

O trabalho foi feito individualmente e foi utilizado como base para implementar o trabalho o código disponibilizado no moodle do servidor select. O código foi totalmente escrito em C.

# Descrição da solução

O binário espera o modo como parâmetro de execução. Baseado nisso vai ficar infinitamente executando o menu da aplicação.

O servidor cria as salas virtuais como threads separadas, visto que cada uma delas fica em loop infinito. O arquivo "definitions.h" possui definições default que estão sendo utilizadas. É definido o IP de forma estática nesse arquivo, já que o servidor e o cliente vão precisar se conectar no mesmo IP. Isso foi feito para simplificar a implementação e os testes, uma entrada a menos de usuário evita falhas. A Porta inicial também está definida nesse arquivo. As salas serão criadas de modo crescente: primeira sala na porta 1200, segunda na 1201, terceira na 1202 e assim por diante.

Quando a opção de sair do servidor é escolhida, todas as threads criadas para as salas virtuais são finalizadas por meio de um envio de sinal.

As salas possuem um limite de pessoas. Esse limite é definido no arquivo "definitions.h" como ROOM\_LIMIT. Quando um cliente tentar entrar numa sala cheia, ele receberá uma mensagem indicando que a sala está cheia. Para realizar isso, mesmo quando a sala está cheia o servidor ainda aceita conexões. Então o servidor aceita a conexão, envia a mensagem avisando que está lotada e encerra a conexão.

# Conclusão

A aplicação permite a criação de centenas de salas e permite a conexão de centenas de clientes. A opção de listar participantes está incompleta, causa problemas por causa de data racing (provavelmente é um problema possível de resolver com o uso de semáforos).

Sobre aprendizados, consegui aprender sobre a aplicação do select, entendi melhor como funciona uma conexão TCP e consegui entender mais sobre sockets no geral.

Sugestões de melhoria:
- Adicionar mais validações para as entradas dos usuários;
- Adicionar mais entradas para customização (IP customizado, portas, limites diferentes de membros para cada sala);
- Funcionalidade de listar participantes das salas (ainda não funciona corretamente);
- Adicionando mais customizações de entrada permitiria a execução de múltiplos servidores em processos separados.

Dificuldades:
- Trabalhar com strings em C é bem complicado. É preciso ficar trabalhando com matrizes sempre que é necessário a criação de um vetor de strings;
- Foi necessário utilizar threads para permitir a criação de múltiplas salas, o que aumentou a complexidade da implementação.

Autoavaliação: participação pessoal é 100% já que é um projeto individual, 80% dos requisitos obrigatórios foram atendidos por completo.

# Referências

* Foi utilizado as manpages das bibliotecas utilizadas no projeto;
* https://stackoverflow.com/questions/16099013/how-to-kill-all-thread-are-running-in-c-language
* https://gitlab.com/sergiosacj/Batalha_Naval
* https://www.baeldung.com/linux/socket-options-difference
* https://stackoverflow.com/questions/21515946/what-is-sol-socket-used-for
* https://stackoverflow.com/questions/24194961/how-do-i-use-setsockoptso-reuseaddr
