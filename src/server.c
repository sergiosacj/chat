#include <arpa/inet.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

#include "definitions.h"
extern ChatRoom chat_rooms[100];

int server(char* ip, int port, int chat_room_id) {
  fd_set main, read_fds;
  struct sockaddr_in myaddr, remoteaddr;
  int fdmax, sd, newfd, i, j, nbytes, yes = 1, limit = 0, room_size = ROOM_LIMIT;
  socklen_t addrlen;
  char message[MAX_LINE_SIZE];
  FD_ZERO(&main);
  FD_ZERO(&read_fds);

  sd = socket(AF_INET, SOCK_STREAM, 0);
  setsockopt(sd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int));

  myaddr.sin_family = AF_INET;
  myaddr.sin_addr.s_addr = inet_addr(ip);
  myaddr.sin_port = htons(port);

  memset(&(myaddr.sin_zero), '\0', 8);
  addrlen = sizeof(remoteaddr);
  if (bind(sd, (struct sockaddr *)&myaddr, sizeof(myaddr)) != 0) {
    printf("Falha ao iniciar o servidor, comando bind retornou erro.\n");
    exit(1);
  }

  listen(sd, room_size + 1);
  FD_SET(sd, &main);
  FD_SET(STDIN, &main);
  fdmax = sd;
  for (;;) {
    read_fds = main;
    select(fdmax + 1, &read_fds, NULL, NULL, NULL);
    for (i = 0; i <= fdmax; i++) {
      if (FD_ISSET(i, &read_fds)) {
        if (i == sd && limit >= room_size) {
          newfd = accept(sd, (struct sockaddr *)&remoteaddr, &addrlen);
          memset(&message, 0, sizeof(message));
          nbytes = recv(newfd, message, sizeof(message), 0);
          char *warning = FULL_ROOM_WARNING;
          write(newfd, warning, strlen(warning));
          limit--;
          close(newfd);
        } else if (i == sd) {
          newfd = accept(sd, (struct sockaddr *)&remoteaddr, &addrlen);
          limit++;
          FD_SET(newfd, &main);
          if (newfd > fdmax)
            fdmax = newfd;

          memset(&message, 0, sizeof(message));
          nbytes = recv(newfd, message, sizeof(message), 0);
          message[nbytes] = '\0';
          memcpy(&chat_rooms[chat_room_id].members[chat_rooms->members_counter++], message, nbytes);
          chat_rooms[chat_room_id].members_counter++;
        }
        else {
          memset(&message, 0, sizeof(message));
          nbytes = recv(i, message, sizeof(message), 0);
          if (strcmp(message, STOP_CONNECTION) == 0) {
            FD_CLR(i, &main);
            limit--;
            close(i);
          } else {
            for (j = 0; j <= fdmax; j++)
              if (FD_ISSET(j, &main))
                if ((j != i) && (j != sd))
                  send(j, message, nbytes, 0);
          }
        }
      }
    }
  }
  return (0);
}
