#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "menu.h"

int main(int argc, char *argv[])
{
  if (argc < 2) {
    printf("Parâmetro obrigatório: Modo (client | server).\n");
    exit(1);
  }

  char *mode = argv[1];

  if (strcmp(mode, "server") == 0)
    while (1)
      drawMenu(1);
  else if (strcmp(mode, "client") == 0)
    while (1)
      drawMenu(0);
  else
    printf("O modo %s não existe. Tente \"client\" ou \"server\"\n", argv[1]);
}
