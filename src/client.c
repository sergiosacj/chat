#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <time.h>

#include "definitions.h"
#include "client.h"

extern char login[100];

int client(char* ip, int port)
{
  int run = 1;
  fd_set main, readers_fd;
  int fd, fdmax;
  char message[MAX_LINE_SIZE], input[MAX_LINE_SIZE];
  struct sockaddr_in servaddr;
  FD_ZERO(&main);
  FD_ZERO(&readers_fd);

	if ((fd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
		printf("Falha ao criar socket\n");
    sleep(1);
    return 1;
	}

	memset(&servaddr, 0, sizeof(servaddr));
	servaddr.sin_family = AF_INET;
	servaddr.sin_port = htons(port);
	servaddr.sin_addr.s_addr = inet_addr(ip);

	if (connect(fd, (struct sockaddr*) &servaddr, sizeof(servaddr)) < 0) {
		printf("Falha ao se conectar ao servidor!\nIP: %s\nPorta: %d\n", ip, port);
    sleep(1);
    return 1;
  }
  printf("Conexão estabelecida!\nIP: %s\nPorta: %d\n", ip, port);

  write(fd, login, sizeof(login));

  FD_SET(fd, &main);
  FD_SET(STDIN, &main);

  fdmax = fd;
  while (run) {
    readers_fd = main;
    select(fdmax + 1, &readers_fd, NULL, NULL, NULL);
    for (int i = 0; i <= fdmax; i++) {
      if (FD_ISSET(i, &readers_fd)) {
        if (i == fd) {
          memset(message, 0, sizeof(message));
          read(fd, message, sizeof(message));
          printf(message);
          if (strcmp(message, FULL_ROOM_WARNING) == 0) {
            printf("Finalizando...\n");
            close(fd);
            run = 0;
            sleep(1);
            break;
          }
        } else {
          memset(input, 0, sizeof(input));
          fgets(input, sizeof(input), stdin);
          if (strcmp(input, "\n") == 0)
            continue;
          if (strcmp(input, STOP_CONNECTION) == 0) {
            printf("Finalizando...\n");
            write(fd, input, sizeof(input));
            close(fd);
            run = 0;
            sleep(1);
            break;
          }

          memset(message, 0, sizeof(message));
          int login_size = strlen(login);
          memcpy(message, login, login_size);
          message[login_size] = ':';
          message[login_size + 1] = ' ';
          memcpy(&message[login_size + 2], input, strlen(input));
          write(fd, message, sizeof(message));
        }
      }
    }
  }
}
