#include <stdio.h>
#include <pthread.h>
#include <signal.h>
#include <time.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "client.h"
#include "server.h"
#include "definitions.h"
#include "menu.h"

int menu_options, chat_rooms_counter, server_ports = PORT, chat_room_number;
char login[100];
ChatRoom chat_rooms[100];

void handleServerOptions();
void handleClientOptions();
void* createChatRoom(void *args);
void clearScreen();

void drawMenu(int is_server) {
  clearScreen();
  printf("MENU:\n\n");

  if (is_server) {
    printf("1. Criar sala virtual.\n");
    printf("2. Listar participantes de sala virtual.\n");
    printf("3. Sair.\n\n");
  } else {
    printf("1. Login.\n");
    printf("2. Entrar numa sala virtual.\n");
    printf("3. Sair.\n\n");
  }
  printf("Escolha: ");
  scanf("%d", &menu_options);

  if (is_server)
    handleServerOptions();
  else
    handleClientOptions();
}

void handleServerOptions() {
  clearScreen();
  switch (menu_options) {
    case 1:
      printf("Criando sala %d com IP %s e PORTA %d\n", chat_rooms_counter, IP, server_ports);
      pthread_create(&chat_rooms[chat_rooms_counter].thread_id, NULL, createChatRoom, &chat_rooms_counter);
      chat_rooms_counter++;
      break;
    case 2:
      printf("Existem %d salas virtuais, qual deseja visualizar?\n", chat_rooms_counter);
      printf("Escolha entre 0 e %d-1: \n", chat_rooms_counter);
      scanf("%d", &chat_room_number);
      if (chat_room_number < chat_rooms_counter) {
        printf("Quantidade de membros do chat %d: %d\n", chat_room_number, chat_rooms[chat_room_number].members_counter);
        printf("Lista de membros do chat %d:\n", chat_room_number);
        for (int i = 0; i < chat_rooms[chat_room_number].members_counter; i++)
          printf("i = %d, member = %s\n", i, chat_rooms[chat_room_number].members[i]);
      } else {
        printf("Essa sala virtual não existe!\n");
      }
      break;
    default:
      printf("Encerrando servidor...\n");
      for (int i = 0; i < chat_rooms_counter; i++)
        pthread_kill(chat_rooms[i].thread_id, SIGINT);
      exit(0);
  }

  printf("Digite 'q' para sair.\n");
  scanf("\n%*c");
}

void handleClientOptions() {
  clearScreen();
  switch (menu_options) {
    case 1:
      printf("Digite o login que irá usar: ");
      scanf("%s", login);
      break;
    case 2:
      if (strcmp(login, "") == 0) {
        printf("É obrigatório realizar o cadastro do login!\n");
        sleep(1);
        break;
      }
      int chat_room_port;
      printf("Digite a PORTA da sala que deseja entrar: ");
      scanf("%d", &chat_room_port);
      client(IP, chat_room_port);
      break;
    default:
      printf("Saindo...\n");
      exit(0);
  }
}

void* createChatRoom(void *args) {
  int *chat_id = args;
  server(IP, server_ports++, *chat_id);
  return 0;
}

void clearScreen() {
  printf("\033[2J\033[1;1H");
}
